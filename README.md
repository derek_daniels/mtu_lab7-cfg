Local Puppet Config Repo
=================

## About

This folder contains the configuration that puppet extects to be able to find on our Lab Image.

## Modifications

If something needs to be added to the lab image the following steps must be taken -- do NOT edit any of the files in this folder on the clients.

1. Fork https://bitbucket.org/mtu-linux/mtu_lab7-cfg
2. Create a new branch with a name like add-tmux
3. Create a pull request back to the mtu remote -- to the develop branch
4. A member of the Linux Team will review your request
5. After testing a new config rpm will be created and placed into the mtu-lab7 channel
6. As part of our regular patch cycle the package will slowly be promoted up through the channels

